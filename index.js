'use strict';
var twitch = require('tmi.js');
var pipndip = "pipndip";
var config = {
    options: {
        debug: true
    },
    connection: {
        cluster: 'aws'
    },
    identity: {
        username: 'phoen1kbot',
        password: 'SECRET'
    },
    channels: [pipndip]
};

var all_names = [];
var challenges = new Map();
var rex_challenge = /^\!challenge @?(\w+)$/
var rex_roll = /^!challengeroll$/

var client = new twitch.client(config);
client.connect();

client.on('chat', (channel, userstate, message, self) => {
    if(self) return;

    if(rex_challenge.test(message) && userstate.username == 'phoen1k'){
        let challenge = {
            challenger: userstate.username,
            challenger_roll: undefined,
            challenged: rex_challenge.exec(message)[1].toLowerCase(),
            challenged_roll: undefined
        };

        console.log(challenge);

        let first = (Math.random() <= 0.5) ? challenge.challenger : challenge.challenged;

        challenges.set(first, challenge);

        client.say(
            pipndip,
            `Oh snap! @${challenge.challenged}, @${challenge.challenger} has challenged you to a roll off! @${first} is going first. Use !challengeroll to roll!`
        );

    }else if(rex_challenge.test(message)){
        client.say(
            pipndip,
            `Why would I listen to you ${userstate.username}? You actually think I'd listen to anyone other than our lord and savior @phoen1k?`
        );

    }else if(rex_roll.test(message)){

        let challenge = challenges.get(userstate.username);

        console.log(challenge);

        if(!!challenge){

            let roll = (userstate.username == 'brooke_e1') ? 0 : Math.floor((Math.random() * 100) + 1);
            let challenger = challenge.challenger;
            let challenged = challenge.challenged;
            let roller = (challenger == userstate.username) ? challenger : challenged;
            let next = (challenger == userstate.username) ? challenged : challenger;

            console.log(roller);
            console.log(next);

            client.say(
                pipndip,
                `Looks like @${roller} rolled... ${roll}!`
            )

            if(challenger == userstate.username){
                challenge.challenger_roll = roll;
            }else{
                challenge.challenged_roll = roll;
            }

            console.log(challenge);

            if(!!challenge.challenger_roll && !!challenge.challenged_roll){
                let winner = (challenge.challenger_roll > challenge.challenged_roll) ? challenger : challenged;
                let loser = winner == challenger ? challenged : challenger;

                client.say(
                    pipndip,
                    `Looks like @${winner} defeated @${loser}. And won... absolutely nothing!!!`
                );

                challenges.delete(roller);

            }else{
                
                challenges.delete(roller);
                challenges.set(next, challenge);

                client.say(
                    pipndip,
                    `You're next @${next}... use !challengeroll to roll!`
                )

            }

        }else{
            client.say(
                pipndip,
                `Doesn't look like you've been challenged @${userstate.username}... or it's not your turn.`
            )
        }

    }

});